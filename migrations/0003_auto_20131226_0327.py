# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):
    
    dependencies = [('shop', '0002_auto_20131226_0302')]

    operations = [
        migrations.AlterField(
            model_name = 'product',
            field = models.FilePathField(path='./resource/lg', null=True),
            name = 'large_image',
        ),
        migrations.AlterField(
            model_name = 'product',
            field = models.FilePathField(path='./resource/tmb', null=True),
            name = 'small_image',
        ),
    ]
