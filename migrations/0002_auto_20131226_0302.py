# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):
    
    dependencies = [('shop', '0001_initial')]

    operations = [
        migrations.AddField(
            name = 'small_image',
            preserve_default = False,
            model_name = 'product',
            field = models.FilePathField(path='./resource/tmb', default=''),
        ),
        migrations.AddField(
            name = 'large_image',
            preserve_default = False,
            model_name = 'product',
            field = models.FilePathField(path='./resource/lg', default=''),
        ),
    ]
