# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):
    
    dependencies = [('shop', '0003_auto_20131226_0327')]

    operations = [
        migrations.AlterField(
            name = 'large_image',
            model_name = 'product',
            field = models.FilePathField(blank=True, null=True, path='./resource/lg'),
        ),
        migrations.AlterField(
            name = 'small_image',
            model_name = 'product',
            field = models.FilePathField(blank=True, null=True, path='./resource/tmb'),
        ),
    ]
