# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):
    
    dependencies = []

    operations = [
        migrations.CreateModel(
            bases = (models.Model,),
            fields = [('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True),), ('name', models.CharField(max_length=30),), ('author', models.CharField(max_length=30),), ('publisher', models.CharField(max_length=30),), ('category', models.CharField(max_length=15),), ('price', models.DecimalField(max_digits=6, decimal_places=2),), ('number', models.DecimalField(max_digits=6),), ('availability', models.BooleanField(),)],
            options = {},
            name = 'Product',
        ),
    ]
