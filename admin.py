from django.contrib import admin
from shop.models import Product

class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','author','publisher','price')
    list_filter = ('name','author')
    search_fields = ('name','author','publisher')

admin.site.register(Product,ProductAdmin)