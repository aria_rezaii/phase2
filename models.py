from django.db import models

class Product(models.Model):
    name = models.CharField(max_length=30)
    author = models.CharField(max_length=30)
    publisher = models.CharField(max_length=30)
    category = models.CharField(max_length=15) # to be changed into a many-to-one relationship with a category object
    price = models.DecimalField(max_digits=6,decimal_places=2)
    number = models.DecimalField(max_digits=6,decimal_places=0)
    small_image = models.FilePathField(path='./resource/tmb', null=True, blank=True)
    large_image = models.FilePathField(path='./resource/lg', null=True, blank=True)
    availability = models.BooleanField()